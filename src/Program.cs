﻿namespace TopShelfDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            TopShelfHelper
                .ServiceCreator<SimpleTaskService>(
                    "TopShelfDemo",
                    "Top Shelf Demo - SimpleTaskService",
                    "Why would you ever create a Windows service without TopShelf?",
                    args);

            /*
            TopShelfHelper
                .ServiceCreator<SimpleTimerService>(
                    "TopShelfDemo",
                    "Top Shelf Demo - SimpleTimerService",
                    "Why would you ever create a Windows service without TopShelf?",
                    args);
            
            TopShelfHelper
                .ServiceCreator<SimpleService>(
                    "TopShelfDemo", 
                    "Top Shelf Demo - SimpleService",
                    "Why would you ever create a Windows service without TopShelf?",
                    args);
            */
        }
    }
}
