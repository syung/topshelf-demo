﻿using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace TopShelfDemo
{
    public class SimpleTaskService : IWindowsService
    {
        private readonly CancellationTokenSource _cancellationTokenSource;
        private readonly CancellationToken _cancellationToken;
        private readonly Task _task;
        private readonly ILog _logger = LogManager.GetLogger(typeof(SimpleTaskService));

        public SimpleTaskService()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = _cancellationTokenSource.Token;

            _task = new Task(DoWork, _cancellationToken);
        }

        public void Start()
        {
            _logger.Info("SimpleTaskService has started.");

            _task.Start();
        }

        public void Stop()
        {
            _cancellationTokenSource.Cancel();
            _task.Wait();

            _logger.Info("SimpleTaskService has stopped.");
        }

        private void DoWork()
        {
            const int maxStep = 5;

            while (!_cancellationTokenSource.IsCancellationRequested)
            {
                for (var i = 0; i < maxStep; i++)
                {
                    _logger.Debug(
                        string.Format("Step {0} of {1}", i + 1, maxStep));
                    Thread.Sleep(1000);
                }
            }
        }
    }
}
