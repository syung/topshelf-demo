﻿using System.IO;
using log4net.Config;
using Topshelf;

namespace TopShelfDemo
{
    public static class TopShelfHelper
    {
        public static void ServiceCreator<T>(string name, string displayName, string description, string[] args) where T : IWindowsService, new()
        {
            HostFactory.Run(hostConfig =>
            {
                hostConfig.Service<IWindowsService>(serviceConfig =>
                {
                    serviceConfig.ConstructUsing(sc => new T());
                    serviceConfig.WhenStarted(s =>
                    {
                        XmlConfigurator.ConfigureAndWatch(new FileInfo(".\\log4net.config"));
                        s.Start();
                    });
                    serviceConfig.WhenStopped(s => s.Stop());
                });

                hostConfig.RunAsLocalSystem();
                hostConfig.SetDescription(description);
                hostConfig.SetDisplayName(displayName);
                hostConfig.SetServiceName(name);
            });
        }   
    }
}
