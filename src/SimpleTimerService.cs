﻿using System;
using System.Timers;
using log4net;

namespace TopShelfDemo
{
    public class SimpleTimerService : IWindowsService
    {
        private readonly Timer _timer;
        private readonly ILog _logger = LogManager.GetLogger(typeof (SimpleTimerService));

        public SimpleTimerService()
        {
            const double interval = 5000;

            _timer = new Timer(interval);
            _timer.Elapsed += OnTick;
        }

        public void Start()
        {
            _logger.Info("SimpleTimerService has started.");

            _timer.AutoReset = true;
            _timer.Enabled = true;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.AutoReset = false;
            _timer.Enabled = false;

            _logger.Info("SimpleTimerService has stopped.");
        }

        protected virtual void OnTick(object sender, ElapsedEventArgs e)
        {
            _logger.Debug("Tick: " + DateTime.Now.ToLongTimeString());
        }
    }
}
