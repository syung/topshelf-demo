﻿namespace TopShelfDemo
{
    public interface IWindowsService
    {
        void Start();
        void Stop();
    }
}
