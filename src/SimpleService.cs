﻿using System;

namespace TopShelfDemo
{
    public class SimpleService : IWindowsService
    {
        public void Start()
        {
            Console.WriteLine("SimpleService has started.");

            while (true)
            {
                Console.WriteLine("Tick: " + DateTime.Now.ToLongTimeString());
                System.Threading.Thread.Sleep(5000);
            }
        }

        public void Stop()
        {
            Console.WriteLine("SimpleService has stopped.");
        }
    }
}
